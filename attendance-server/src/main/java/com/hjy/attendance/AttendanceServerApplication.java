package com.hjy.attendance;

import com.gitee.sophis.annotations.openApi;
import com.hjy.attendance.annotations.OpenAip;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @description: 启动类
 * @author: xjr
 * @create: 2020-04-18 23:08
 **/

@SpringBootApplication
@openApi(title = "人脸识别平台后端接口在线文档",description = "基于人脸识别的课堂考勤管理平台在线API文档")
@MapperScan("com.hjy.attendance.dao")
@ServletComponentScan
@OpenAip
public class AttendanceServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(AttendanceServerApplication.class,args);
    }

    /*
    开启websocket支持
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
}
