package com.hjy.attendance.cache;

import com.hjy.attendance.entity.User;
import com.hjy.attendance.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: attendance-parent
 * @description: 考勤定时任务
 * @author: xjr
 * @create: 2020-04-26 17:55
 **/
@Component
@Slf4j
public class AttendanceTask implements DisposableBean {

    @Autowired
    UserService userService;

    /**
     *
     * @description 方法描述 暂时预留。 为后面的考勤作为准备
     * @return
     * @author xiejiarong
     * @date 2020年04月26日 18:03
     */
    @Override
    public void destroy() throws Exception {
        log.info("程序已退出");
        User user=userService.getById("2020041801");
        log.info("退出之前查询到用户L{}",user);
    }
}
