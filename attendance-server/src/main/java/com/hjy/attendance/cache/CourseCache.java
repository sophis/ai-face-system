package com.hjy.attendance.cache;

import com.hjy.attendance.entity.Course;
import com.hjy.attendance.service.CourseService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: attendance-parent
 * @description: 课程缓存
 * @author: xjr
 * @create: 2020-04-25 17:15
 **/
@Component
public class CourseCache extends AbstractCache<Course, CourseService> {
    @Override
   public  List<Course> get() {
        synchronized (CourseCache.class){
            if (this.cacheList==null|| this.cacheList.size()==0){
                this.cacheList=this.sourceService.list();
            }
        }
        return this.cacheList;
    }


    @Override
    public void clear() {
        synchronized (CourseCache.class){
            this.cacheList.clear();
        }
    }

    @Override
    public void updateById(Course course) {
        synchronized (TeacherCache.class){
            this.cacheList=this.cacheList.stream().map(data->{
                if (course.getCourseNo().equals(data.getCourseNo())){
                    data=course;
                }
                return data;
            }).collect(Collectors.toList());
        }
    }

    @Override
    public Integer getOrder() {
        return 2;
    }
}
