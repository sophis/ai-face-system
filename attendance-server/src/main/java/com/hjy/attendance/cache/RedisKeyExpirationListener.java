package com.hjy.attendance.cache;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hjy.attendance.entity.AttendanceParent;
import com.hjy.attendance.entity.AttendanceSon;
import com.hjy.attendance.entity.Leave;
import com.hjy.attendance.service.AttendanceParentService;
import com.hjy.attendance.service.AttendanceSonService;
import com.hjy.attendance.service.FaceInformationService;
import com.hjy.attendance.service.LeaveService;
import com.hjy.attendance.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: xjr
 * @create: 2019-12-06 16:11
 */
@Component
@Slf4j
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    @Autowired
    AttendanceParentService attendanceParentService;

    @Autowired
    AttendanceSonService attendanceSonService;

    @Autowired
    LeaveService leaveService;

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        // 这里的key实际是失效的考勤主键
        String expiredKey = message.toString();
        AttendanceParent attendanceParent=attendanceParentService.getById(expiredKey);
        long date=attendanceParent.getAllowTimeDifference().getTime();
        attendanceParent.setTotalSuccessFlag(1);//设置已完成
        attendanceParentService.updateById(attendanceParent);
        QueryWrapper<AttendanceSon> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("attentance_parent_id",expiredKey);
        List<AttendanceSon> attendanceSons=attendanceSonService.list(queryWrapper);
        attendanceSons=attendanceSons.stream().peek(data->{
            if (data.getSuccessFlag()==0){
                data.setSuccessFlag(1);
                data.setAttentanceFlag(2);
            }
        }).collect(Collectors.toList());
        this.attendanceSonService.updateBatchById(attendanceSons);
    }
}
