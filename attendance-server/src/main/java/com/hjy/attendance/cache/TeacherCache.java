package com.hjy.attendance.cache;

import com.hjy.attendance.entity.Teacher;
import com.hjy.attendance.service.TeacherService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: attendance-parent
 * @description: 教师缓存
 * @author: xjr
 * @create: 2020-04-25 15:43
 **/
@Component
public class TeacherCache extends AbstractCache<Teacher, TeacherService> {

    @Override
    public List<Teacher> get() {
        synchronized (TeacherCache.class){
            if (this.cacheList==null|| this.cacheList.size()==0){
                this.cacheList=this.sourceService.list();
            }
        }
        return this.cacheList;
    }

    @Override
    public void clear() {
        synchronized (TeacherCache.class){
            this.cacheList.clear();
        }
    }

    @Override
    public void updateById(Teacher teacher) {
        synchronized (TeacherCache.class){
            this.cacheList=this.cacheList.stream().map(data->{
                if (teacher.getGuid().equals(data.getGuid())){
                    data=teacher;
                }
                return data;
            }).collect(Collectors.toList());
        }

    }

    @Override
    public Integer getOrder() {
        return 0;
    }
}
