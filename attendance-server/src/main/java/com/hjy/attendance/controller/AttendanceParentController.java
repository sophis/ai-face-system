package com.hjy.attendance.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.entity.AttendanceParent;
import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.AttendanceParentService;
import com.hjy.attendance.util.BeanUtil;
import com.hjy.attendance.util.JwtUtil;
import com.hjy.attendance.util.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;


/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/attendanceParent")
@Api(tags = "考勤主表服务接口")
        public class AttendanceParentController extends BaseController<AttendanceParentService, AttendanceParent> {
        @Autowired
        RedisUtil redisUtil;

        @PostMapping("/publish")
        @ApiOperation("教师发布课程")
        public ResultVO publish(@RequestBody String attendanceParent) throws ApiException {
                JSONObject jsonObject= JSON.parseObject(attendanceParent);
                jsonObject.put("content",jsonObject.get("content").toString().getBytes());
                this.service.publish(JSON.parseObject(jsonObject.toJSONString(),AttendanceParent.class));
                return ResultVO.success("发布成功");
        }


        @GetMapping("/getUnCompletedAttendances")
        @ApiOperation("获取当前未完成考勤")
        public ResultVO getUnCompletedAttendances() throws Exception{
                return ResultVO.data(this.service.getUnCompletedAttendances());
        }


        @GetMapping("/getAttendanceInfo")
        @ApiOperation("考勤管理列表数据获取")
        public ResultVO getAttendanceInfo() throws Exception{
                return ResultVO.data(this.service.getAttendanceInfo());
        }

        @GetMapping("/getTeacherCourseStudents")
        @ApiOperation("获取教师课程底下所有选修的学生")
        public ResultVO getTeacherCourseStudents() throws Exception{
                return ResultVO.data(this.service.getTeacherCourseStudents(JwtUtil.decode(IMContextHolder.get("token"))));
        }

        }

