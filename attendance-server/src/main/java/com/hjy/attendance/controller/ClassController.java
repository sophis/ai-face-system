package com.hjy.attendance.controller;


import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.Class;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.service.ClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>标题: 班级表控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/class")
@Api(tags = "班级表服务接口")
        public class ClassController extends BaseController<ClassService, Class> {

        @GetMapping("/getAllClasses")
        @ApiOperation("获得所有班级")
        public ResultVO getAllClasses(){
                return ResultVO.data(service.getAllClasses());
        }


        }

