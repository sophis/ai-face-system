package com.hjy.attendance.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hjy.attendance.cache.CourseCache;
import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.Course;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * <p>标题: 课程表控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/course")
@Api(tags = "课程表服务接口")
        public class CourseController extends BaseController<CourseService, Course> {

        @Autowired
        CourseCache courseCache;
        @GetMapping("/getCourseListByKeyWords")
        @ApiOperation("根据课程名和任课教师姓名模糊搜索课程列表")
        public ResultVO<List<Map<String, Object>>> getCourseListByKeyWords(@ApiParam("课程名") String courseName, @ApiParam("教师名") String teacherName) throws Exception {
                return ResultVO.data(this.service.getCourseListByKeyWords(courseName, teacherName));
        }

        @PostMapping("/updateByCourseNo")
        @ApiOperation("根据课程号更新课程信息")
        public ResultVO updateByCourseNo(@RequestBody Course course) throws Exception{
                this.service.updateById(course);
                this.courseCache.clear();
                return ResultVO.success("更新成功");
        }

        @DeleteMapping("/deleteOneCourse/{id}")
        @ApiOperation("删除一条课程,教师端操作")
        public ResultVO deleteOneCourse(@PathVariable Serializable id){
                this.service.deleteById(id);
                this.courseCache.clear();
                return ResultVO.success("删除成功");
        }


        @PostMapping("/publish")
        @ApiOperation("管理员和老师发布课程")
        public ResultVO publish(@RequestBody String param) throws ApiException {
                JSONObject requestparam= JSON.parseObject(param).getJSONObject("data");
                this.service.publish(requestparam);
                return ResultVO.success("发布成功");
        }

        @DeleteMapping("/deleteAllByCourseNo/{courseNo}")
        @ApiOperation("删除该课程并且删除所有考勤信息")
        public ResultVO deleteAllByCourseNo(@PathVariable String courseNo) throws Exception{
                this.service.deleteAllByCourseNo(courseNo);
                return ResultVO.success("删除成功");
        }



}