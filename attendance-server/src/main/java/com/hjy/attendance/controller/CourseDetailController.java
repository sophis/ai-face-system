package com.hjy.attendance.controller;


import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.service.CourseDetailService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;


import com.hjy.attendance.entity.CourseDetail;

import org.springframework.web.bind.annotation.RestController;



/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: aa
 * @date 2020-04-29
 */
@RestController
@RequestMapping("/ai_face/attendance/courseDetail")
@Api(tags = "控制层")
        public class CourseDetailController extends BaseController<CourseDetailService,CourseDetail> {




        }

