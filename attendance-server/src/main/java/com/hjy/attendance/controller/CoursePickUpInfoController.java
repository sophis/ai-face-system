package com.hjy.attendance.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.CoursePickUpInfo;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.CoursePickUpInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/coursePickUpInfo")
@Api(tags = "选修课程表服务接口")
        public class CoursePickUpInfoController extends BaseController<CoursePickUpInfoService, CoursePickUpInfo> {

        @GetMapping("/getCoursesAndStudents")
        @ApiOperation("获取选课的信息和学生信息")
        public ResultVO getCoursesAndStudents() throws ApiException {
                return ResultVO.data(this.service.getCoursesAndStudents());

        }

    @GetMapping("/getMyCourses")
    @ApiOperation("查看自己当前所有的选课课程详细信息")
        public ResultVO getMyCourses() throws Exception{
            return ResultVO.data(this.service.getMyCourses());
        }

        @GetMapping("/getPickUpListByCourse")
        @ApiOperation("根据课程号查看该课程下所有选修情况")
        public ResultVO getPickUpListByCourse(String courseNo) throws Exception{
            return ResultVO.data(service.getPickUpListByCourse(courseNo));
        }
        }

