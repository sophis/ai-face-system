package com.hjy.attendance.controller;


import cn.hutool.core.codec.Base64Encoder;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baidu.aip.face.AipFace;
import com.hjy.attendance.ai.AiFaceTemplate;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.FaceInformation;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.entity.User;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.FaceInformationService;
import com.hjy.attendance.util.WebUtils;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.InputStream;
import java.util.Base64;
import java.util.HashMap;

/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/faceInformation")
@Api(tags = "人脸表服务接口")
        public class FaceInformationController extends BaseController<FaceInformationService, FaceInformation> {

        @Autowired
        AiFaceTemplate aiFaceTemplate;
        @PostMapping("/addFace")
        @ApiOperation("人脸注册")
        public ResultVO addFace(@RequestParam MultipartFile file, @RequestParam String user) throws Exception {
                User targetUser= JSON.parseObject(user,User.class);
                return ResultVO.data(this.service.addUser(file,targetUser));
        }

        @PostMapping("/checkFaceByImage")
        @ApiOperation("人脸比对")
        public ResultVO checkFace(@RequestParam @ApiParam("比对的图片") MultipartFile file, @RequestParam @ApiParam("参与比对的用户ID") String compareId) throws Exception{
                return ResultVO.data(this.service.checkFace(file,compareId));
        }

        @PostMapping("/judgeFace")
        @ApiOperation("人脸比对,通过前端生成的base64图片编码")
        public ResultVO judgeFace(@RequestBody String image,@RequestParam("attendanceId") String attendanceId) throws ApiException{
                JSONObject img=JSON.parseObject(image);
                return ResultVO.data(this.service.checkFace(img.get("data").toString(),attendanceId));
        }

        @PostMapping("/judgeBeforePublishLeave")
        @ApiOperation("在发布假条之前校验人脸")
        public ResultVO judgeBeforePublishLeave(@RequestBody String image,@RequestParam("studentNo")String studentNo) throws Exception{
                JSONObject img=JSON.parseObject(image);
                return ResultVO.data(this.service.judgeBeforePublishLeave(img.get("data").toString(),studentNo));
        }

        @PostMapping("/searchFace")
        @ApiOperation("单张人脸进行搜索,实现轮流自动考勤功能")
        public ResultVO searchFace(@ApiParam("上传的人脸") MultipartFile file) throws Exception{
                WebUtils.getCurrentSession().setAttribute("operator","xiexie");
                return ResultVO.data(this.service.serachFace(file));
        }


        @PostMapping("/compareWithTwoImh")
        @ApiOperation("对比两张人脸")
        public String compareWithTwoImh(MultipartFile file1,MultipartFile file2) throws Exception{
                String[] res=AiFaceTemplate.stream2Base64(file1,file2);
                return aiFaceTemplate.compareWithTwoImh(res[0],res[1]);
        }

        @GetMapping("/getALlFaces")
        @ApiOperation("获取所有人脸及用户信息")
        public ResultVO getALlFaces() throws Exception{
                return ResultVO.data(service.getALlFaces());
        }


        @ApiOperation("新增人脸")
        @PostMapping("/addFaceToDataBase")
        public ResultVO addFace(@RequestBody String param) throws Exception {
                JSONObject jsonObject=JSON.parseObject(param).getJSONObject("data");
                service.addFace(jsonObject);
                return ResultVO.success("更新成功");
        }




        }

