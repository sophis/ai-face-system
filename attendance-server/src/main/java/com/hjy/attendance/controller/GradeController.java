package com.hjy.attendance.controller;


import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.Grade;
import com.hjy.attendance.service.GradeService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>标题: 年级表控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/grade")
@Api(tags = "年级表服务接口")
        public class GradeController extends BaseController<GradeService, Grade> {
    



        }

