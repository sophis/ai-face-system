package com.hjy.attendance.controller;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hjy.attendance.entity.*;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.LeaveService;
import com.hjy.attendance.util.BeanUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/leave")
@Api(tags = "请假表服务接口")
public class LeaveController extends BaseController<LeaveService, Leave> {


        @PostMapping("getByCondition")
        public ResultVO getLeavesByCondition(@RequestBody Map<String,Object> map) throws ApiException {
                List<Map<String,Object>> leaves = this.service.getByCondition(map);
                return ResultVO.<List<Map<String,Object>>>success().message("成功了").data(leaves).build();
        }
        @PostMapping("agree")
        public ResultVO agree(@RequestBody Leave leave) throws Exception {
                this.service.agree(leave);
                return ResultVO.success("成功");
        }
        @PostMapping("disagree")
        public ResultVO disagree(@RequestBody Leave leave) throws Exception {
                this.service.disagree(leave);
                return ResultVO.success("成功");
        }

        @PostMapping("/publish")
        @ApiOperation("发起请假")
        public ResultVO publish(@RequestBody String param) throws Exception{
                JSONObject res= JSON.parseObject(param);
                service.publish(res);
                return ResultVO.success("发布成功");
        }

        @GetMapping("/getAllLeaves")
        @ApiOperation("学生端获得所有请假信息")
        public ResultVO getAllLeaves(String studentNo) throws Exception{
                return ResultVO.data(service.getAllLeaves(studentNo));
        }

}

