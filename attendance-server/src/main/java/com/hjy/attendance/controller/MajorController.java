package com.hjy.attendance.controller;


import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.Major;
import com.hjy.attendance.service.MajorService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/major")
@Api(tags = "专业表服务接口")
        public class MajorController extends BaseController<MajorService, Major> {
    



        }

