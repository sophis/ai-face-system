package com.hjy.attendance.controller;


import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.entity.Student;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/student")
@Api(tags = "学生表服务接口")
        public class StudentController extends BaseController<StudentService, Student> {

        @GetMapping("/getAssiantByUserName/{userId}")
        @ApiOperation("通过学生账号获得辅导员信息")
        public ResultVO getAssiantByUserName(@PathVariable String userId) throws ApiException {
                return ResultVO.data(service.getAssiantByUserName(userId));
        }


        }

