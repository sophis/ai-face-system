package com.hjy.attendance.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.entity.BaseController;
import com.hjy.attendance.entity.ResultVO;
import com.hjy.attendance.entity.User;
import com.hjy.attendance.service.UserService;
import com.hjy.attendance.util.WebUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * <p>标题: 控制层</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@RestController
@RequestMapping("/ai_face/ai-face-attendance/user")
@Api(tags = "用户表服务接口")
public class UserController extends BaseController<UserService, User> {

        @PostMapping("/loginByInput")
        @ApiOperation("普通表单登录")
        public ResultVO login(@RequestBody User user) throws Exception{
                return ResultVO.<User>data(this.service.loginByForm(user));
        }

        @GetMapping("/refreshUserByToken")
        @ApiOperation(value = "根据token来获取用户信息",
        notes = "主要使用场景是用户刷新浏览器后的vuex用户信息丢失")
        public ResultVO refreshUserByToken() throws Exception {
                return ResultVO.<Map>data(service.refreshUserByToken());
        }


        @DeleteMapping("/logOut")
        @ApiOperation(value = "用户退出")
        public ResultVO logOut() throws Exception {
                WebUtils.getCurrentSession().removeAttribute(IMContextHolder.get(IMContextHolder.OPERATOR));
                IMContextHolder.clear();
                return ResultVO.data("success");
        }

        @GetMapping("/getAllUsers")
        @ApiOperation("获取所有用户")
        public ResultVO getAllUsers() throws Exception{
                return ResultVO.data(service.getAllUsers());
        }

        @PostMapping("/addUser")
        @ApiOperation("新增用户")
        public ResultVO addUser(@RequestBody Map<String,Object> user) throws Exception{
                this.service.addUser(user);
                return ResultVO.success("新增成功");

        }






}

