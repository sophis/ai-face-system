package com.hjy.attendance.dao;

import com.hjy.attendance.entity.AttendanceParent;
import com.hjy.attendance.entity.BaseDao;
import com.hjy.attendance.entity.Student;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface AttendanceParentDao extends BaseDao<AttendanceParent> {

    List<Map<String,Object>> getAttendanceInfo() throws Exception;

    List<Student> getTeacherCourseStudents(@Param("teacherId") String teacherId);

}
