package com.hjy.attendance.dao;

import com.hjy.attendance.entity.Class;
import com.hjy.attendance.entity.BaseDao;

import java.util.List;
import java.util.Map;

/**
 * <p>标题: 班级表 Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface ClassDao extends BaseDao<Class> {

    List<Map<String,Object>> getAllClass();

}
