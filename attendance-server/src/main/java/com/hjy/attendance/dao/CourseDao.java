package com.hjy.attendance.dao;

import com.hjy.attendance.entity.BaseDao;
import com.hjy.attendance.entity.Course;


/**
 * <p>标题: 课程表 Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface CourseDao extends BaseDao<Course> {

}
