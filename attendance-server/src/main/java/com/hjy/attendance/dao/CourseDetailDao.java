package com.hjy.attendance.dao;

import com.hjy.attendance.entity.BaseDao;
import com.hjy.attendance.entity.CourseDetail;


/**
 * <p>标题:  Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: aa
 * @date 2020-04-29
 */
public interface CourseDetailDao extends BaseDao<CourseDetail> {

}
