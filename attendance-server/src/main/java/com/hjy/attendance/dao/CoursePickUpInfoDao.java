package com.hjy.attendance.dao;

import com.hjy.attendance.entity.BaseDao;
import com.hjy.attendance.entity.CoursePickUpInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface CoursePickUpInfoDao extends BaseDao<CoursePickUpInfo> {

    List<Map<String,Object>> getMyCourses(@Param("studentNo") String studentNo);

    List<Map<String,Object>> getPickUpListByCourse(@Param("courseNo")String courseNo) throws Exception;
}
