package com.hjy.attendance.dao;

import com.hjy.attendance.entity.Grade;
import com.hjy.attendance.entity.BaseDao;


/**
 * <p>标题: 年级表 Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface GradeDao extends BaseDao<Grade> {

}
