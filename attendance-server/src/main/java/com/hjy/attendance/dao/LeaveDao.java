package com.hjy.attendance.dao;

import com.hjy.attendance.entity.LeaveVo;
import com.hjy.attendance.entity.Leave;
import com.hjy.attendance.entity.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Dao 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface LeaveDao extends BaseDao<Leave> {

    List<LeaveVo> getByCondition(@Param("map") Map<String, Object> map);

    Leave getInfoById(@Param("pid") String id);

    int updateInfoById(Leave leave);
}
