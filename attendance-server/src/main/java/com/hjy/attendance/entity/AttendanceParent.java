package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("考勤主表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceParent extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "考勤记录主表")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "课程号")
    private String courseNo;

    @ApiModelProperty(value = "发起者")
    private String publishNo;

    @ApiModelProperty(value = "考勤考试时间期望值")
    private Date attendanceExpectStartTime;

    @ApiModelProperty(value = "允许迟到时间")
    private Date allowTimeDifference;

    @ApiModelProperty(value = "ip白名单")
    @TableField("white_ipv4_Address")
    private String whiteIpv4Address;

    @ApiModelProperty(value = "总考勤标识 0 进行中 1已完成",example = "0")
    private Integer totalSuccessFlag;

    @ApiModelProperty(value = "发布时间")
    private Date createTime;

    @ApiModelProperty(value = "考勤内容")
    private byte[] content;


}
