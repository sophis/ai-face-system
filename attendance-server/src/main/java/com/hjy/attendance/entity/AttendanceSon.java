package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("考勤子表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceSon extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "考勤记录细表编号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "考勤主表编号(对应一个课程的一次考勤计划)")
    private String attentanceParentId;

    @ApiModelProperty(value = "学生学号")
    private String userId;

    @ApiModelProperty(value = "实际考勤时间")
    @TableField("attentance_record__start_time")
    private Date attentanceRecordStartTime;

    @ApiModelProperty(value = "考勤标识 0 失败 1成功",example = "0")
    private Integer successFlag;

    @ApiModelProperty(value = "考勤标识 0 迟到 1正常 2 缺勤 3请假",example = "0")
    private Integer attentanceFlag;

    @ApiModelProperty(value = "考勤ip地址")
    @TableField("ipv4_address")
    private String ipv4Address;


}
