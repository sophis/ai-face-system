package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 班级表
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("班级表")
public class Class extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "班级编号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "班级名")
    private String className;

    @ApiModelProperty(value = "年级编号")
    private String gradeNo;

    @ApiModelProperty(value = "专业编号")
    private String majorCode;

    @ApiModelProperty(value = "院系编码")
    private String departmentCode;


}
