package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * <p>
 * 课程表
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("课程表")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Course extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "课程号")
    @TableId(value = "course_no", type = IdType.UUID)
    private String courseNo;

    @ApiModelProperty(value = "课程名")
    private String courseName;

    @ApiModelProperty(value = "任课教师工号")
    private String teacherId;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "创建者")
    private String createOperator;


}
