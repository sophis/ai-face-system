package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author aa
 * @since 2020-04-29
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("课程明细表")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CourseDetail extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "课程号")
    @TableId(value = "course_no", type = IdType.UUID)
    private String courseNo;

    @ApiModelProperty(value = "课程标题")
    private String title;

    @ApiModelProperty(value = "适合专业")
    private String fitMajor;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "是否有效",example = "1")
    private Integer validFlag;

    @ApiModelProperty(value = "课程概述")
    private byte[] content;


}
