package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("课程选修表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CoursePickUpInfo extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "选课表主键")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "学生学号")
    private String studentNo;

    @ApiModelProperty(value = "课程号")
    private String courseNo;

    @ApiModelProperty(value = "选课标识 0无效 1有效",example = "0")
    private Integer validFlag;


}
