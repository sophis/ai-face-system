package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("字典表")
public class Dict extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "字典标识码")
    @TableId(value = "dict_code", type = IdType.UUID)
    private String dictCode;

    @ApiModelProperty(value = "字典值")
    private String dictValue;

    @ApiModelProperty(value = "有效标识 0 无效 1有效",example = "0")
    private Integer validFlag;

    @ApiModelProperty(value = "父字典项")
    private String  parentCode;


}
