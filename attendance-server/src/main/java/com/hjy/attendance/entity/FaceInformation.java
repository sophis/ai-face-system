package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;
import java.util.Date;

import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("人脸表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FaceInformation extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "人脸ID")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "人脸图像base64密文")
    private byte[] faceContent;

    @ApiModelProperty(value = "人脸所属用户编号")
    private String userId;

    @ApiModelProperty(value = "有效标志 0 无效 1 有效",example = "0")
    private Integer validFlag;

    @ApiModelProperty(value = "匹配分数阈值（管理员自动可以设置）",example = "0")
    @TableField("standard_score")
    private Integer standardScore;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    @TableField("update_time")
    private Date updateTime;


}
