package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 年级表
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("年级表")
public class Grade extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "年级编码")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "关联字典项")
    private String dictCode;

    @ApiModelProperty(value = "字典值")
    private String dictValue;


}
