package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.sql.Blob;

import com.baomidou.mybatisplus.annotation.TableName;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("请假表")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("`leave`")
public class Leave extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "请假记录编号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "学生学号")
    private String studentNo;

    @ApiModelProperty(value = "请假开始时间")
    private Date leaveStartTime;

    @ApiModelProperty(value = "请假结束时间")
    private Date leaveEndTime;

    @ApiModelProperty(value = "审批状态 0 未处理 1 批准 2 驳回",example = "0")
    private Integer validFlag;

    @ApiModelProperty(value = "审批者用户名 (教师为工号，管理员为用户名)")
    private String judgeId;

    @ApiModelProperty(value = "请假事由")
    private byte[] leaveReason;

    @ApiModelProperty(value = "请假者人脸加密信息")
    private byte[] leaveImageContent;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("审核时间")
    private Date judgeTime;




}
