package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Blob;
import java.util.Date;
@Data
public class LeaveVo {
    @ApiModelProperty(value = "请假记录编号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "学生姓名")
    private String studentName;

    @ApiModelProperty(value = "学生编号")
    private String studentNo;

    @ApiModelProperty(value = "请假开始时间")
    private Date leaveStartTime;

    @ApiModelProperty(value = "请假结束时间")
    private Date leaveEndTime;

    @ApiModelProperty(value = "审批状态 0 未处理 1 批准 2 驳回",example = "0")
    private Integer validFlag;

    @ApiModelProperty(value = "审批者用户名 (教师为工号，管理员为用户名)")
    private String judgeName;

    @ApiModelProperty(value = "请假事由")
    private byte[] leaveReason;

    @ApiModelProperty(value = "请假结果")
    private String leaveResult;

}
