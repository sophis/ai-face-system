package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("日志表")
public class Log extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "日志编号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "操作者工号")
    private String userName;

    @ApiModelProperty(value = "操作内容")
    private String logContent;

    @ApiModelProperty(value = "操作时间")
    private Date recordTime;

    @ApiModelProperty(value = "请求来源ip")
    private String ipv4Address;

    @ApiModelProperty(value = "消耗时间,默认ms")
    private String spendTime;

    @Override
    public String toString() {
        return "Log{" +
                "guid='" + guid + '\'' +
                ", userName='" + userName + '\'' +
                ", logContent='" + logContent + '\'' +
                ", recordTime=" + recordTime +
                ", ipv4Address='" + ipv4Address + '\'' +
                ", spendTime='" + spendTime + '\'' +
                '}';
    }
}
