package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("专业表")
public class Major extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "专业编号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "字典值")
    private String dictCode;

    @ApiModelProperty(value = "字典值(专业名)")
    private String dictValue;

    @ApiModelProperty(value = "院系编号")
    private String departmentCode;

    @ApiModelProperty(value = "专业介绍")
    private String brief;


}
