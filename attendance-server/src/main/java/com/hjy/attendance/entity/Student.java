package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("学生表")
public class Student extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "学号")
    @TableId(value = "guid", type = IdType.UUID)
    private String guid;

    @ApiModelProperty(value = "学生姓名")
    private String name;

    private String sex;

    @ApiModelProperty(value = "班级编号")
    private String classNo;

    @ApiModelProperty(value = "年级编号")
    private String gradeNo;

    @ApiModelProperty(value = "专业")
    private String majorNo;

    @ApiModelProperty(value = "院系编码")
    private String departmentNo;

    @ApiModelProperty(value = "辅导员ID")
    private String assistant;

}
