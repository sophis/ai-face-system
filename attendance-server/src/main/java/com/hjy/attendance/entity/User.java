package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.hjy.attendance.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author huangjunyu
 * @since 2020-04-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel("用户表")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {

private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户名")
    @TableId(value = "user_name", type = IdType.UUID)
    private String userName;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "有效标识 0 无效 1有效",example = "0")
    private Integer validFlag;

    @ApiModelProperty(value = "角色权限编码 0管理员 1 学生 2教师")
    private String permissionCode;

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", createdTime=" + createdTime +
                ", updateTime=" + updateTime +
                ", validFlag=" + validFlag +
                ", permissionCode='" + permissionCode + '\'' +
                '}';
    }
}
