package com.hjy.attendance.role;

/**
 * @program: springcloud-dependcies
 * @description: 角色枚举
 * @author: xjr
 * @create: 2020-04-19 18:13
 **/
public enum RoleType {

    TEACHER("teacher"),
    STUDENT("student"),
    ADMIN("admin");

    public String groupId;

    RoleType(String groupId){
        this.groupId=groupId;
    }
}
