package com.hjy.attendance.service;

import com.hjy.attendance.entity.AttendanceParent;
import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.entity.Student;
import com.hjy.attendance.exception.ApiException;

import java.util.List;
import java.util.Map;

/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface AttendanceParentService extends BaseService<AttendanceParent> {

    void publish(AttendanceParent attendanceParent) throws ApiException;

    List<Map<String,Object>> getUnCompletedAttendances() throws Exception;

    List<Map<String,Object>> getAttendanceInfo() throws Exception;

    List<Student> getTeacherCourseStudents(String teacherId) throws Exception;

}
