package com.hjy.attendance.service;

import com.hjy.attendance.entity.Class;
import com.hjy.attendance.entity.BaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>标题: 班级表 Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface ClassService extends BaseService<Class> {

    List<Map<String,Object>> getAllClasses();

}
