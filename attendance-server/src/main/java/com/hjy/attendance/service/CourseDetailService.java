package com.hjy.attendance.service;

import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.entity.CourseDetail;


/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: aa
 * @date 2020-04-29
 */
public interface CourseDetailService extends BaseService<CourseDetail> {

}
