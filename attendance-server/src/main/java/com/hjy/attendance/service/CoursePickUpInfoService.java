package com.hjy.attendance.service;

import com.hjy.attendance.entity.CoursePickUpInfo;
import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.exception.ApiException;
import io.swagger.annotations.Api;

import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface CoursePickUpInfoService extends BaseService<CoursePickUpInfo> {

    List<Map<String,Object>> getCoursesAndStudents() throws ApiException;

    List<Map<String,Object>> getMyCourses() throws  Exception;


    List<Map<String,Object>> getPickUpListByCourse(String courseNo) throws Exception;

}
