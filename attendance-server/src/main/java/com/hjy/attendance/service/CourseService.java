package com.hjy.attendance.service;

import com.hjy.attendance.entity.Course;
import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.exception.ApiException;

import java.util.List;
import java.util.Map;

/**
 * <p>标题: 课程表 Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface CourseService extends BaseService<Course> {
    List<Map<String,Object>> getCourseListByKeyWords(String courseName, String teacherName);

    void publish(Map<String,Object> requestMap) throws ApiException;

    void deleteAllByCourseNo(String courseNo) throws ApiException;

}
