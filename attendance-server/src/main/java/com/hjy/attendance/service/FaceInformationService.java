package com.hjy.attendance.service;

import com.alibaba.fastjson.JSONObject;
import com.hjy.attendance.entity.FaceInformation;
import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.entity.User;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.role.RoleType;
import org.springframework.web.multipart.MultipartFile;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface FaceInformationService extends BaseService<FaceInformation> {

    JSONObject addUser(MultipartFile file, User user) throws Exception;

    JSONObject checkFace(MultipartFile file,String compareId) throws  Exception;

    JSONObject serachFace(MultipartFile file) throws Exception;


    JSONObject checkFace(String base64Img,String attendanceId) throws ApiException;

    List<Map<String,Object>> getALlFaces() throws Exception;

    void addFace(Map<String,Object> param) throws Exception;

    String judgeBeforePublishLeave(String image,String StudentNo) throws Exception;




}
