package com.hjy.attendance.service;

import com.hjy.attendance.entity.LeaveVo;
import com.hjy.attendance.entity.Leave;
import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.exception.ApiException;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface LeaveService extends BaseService<Leave> {

    List<Map<String,Object>> getByCondition(@Param("map") Map<String,Object> map) throws ApiException;

    void agree(Leave leaves) throws Exception;

    void disagree(Leave leaves) throws ApiException;

    void publish(Map<String,Object> param) throws Exception;

    List<Map<String,Object>> getAllLeaves(String studengNo) throws Exception;
}
