package com.hjy.attendance.service;

import com.hjy.attendance.entity.Log;
import com.hjy.attendance.entity.BaseService;

/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface LogService extends BaseService<Log> {

    int saveLog(String jsonLog) throws Exception;

}
