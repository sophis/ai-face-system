package com.hjy.attendance.service;

import com.hjy.attendance.entity.User;
import com.hjy.attendance.entity.BaseService;
import com.hjy.attendance.exception.ApiException;

import java.util.List;
import java.util.Map;


/**
 * <p>标题:  Service 接口</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
public interface UserService extends BaseService<User> {

    User loginByForm(User user) throws Exception;

    Map<String,Object> refreshUserByToken() throws Exception;

    List<Map<String,Object>> getAllUsers() throws Exception;

    void addUser(Map<String,Object> user) throws ApiException;

}
