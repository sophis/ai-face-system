package com.hjy.attendance.service.impl;

import com.hjy.attendance.dao.ClassDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Class;
import com.hjy.attendance.service.ClassService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/**
 * <p>标题: 班级表 Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class ClassServiceImpl extends BaseServiceImpl<ClassDao, Class> implements ClassService {

    @Override
    public List<Map<String, Object>> getAllClasses() {
        return this.baseDao.getAllClass();
    }
}
