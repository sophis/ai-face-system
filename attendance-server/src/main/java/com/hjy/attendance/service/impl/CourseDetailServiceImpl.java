package com.hjy.attendance.service.impl;

import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.CourseDetail;

import com.hjy.attendance.service.CourseDetailService;
import com.hjy.attendance.dao.CourseDetailDao;
import org.springframework.stereotype.Service;


/**
 * <p>标题:  Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: aa
 * @date 2020-04-29
 */
@Service
public class CourseDetailServiceImpl extends BaseServiceImpl<CourseDetailDao, CourseDetail> implements CourseDetailService {

}
