package com.hjy.attendance.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.dao.CoursePickUpInfoDao;
import com.hjy.attendance.entity.CoursePickUpInfo;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.CoursePickUpInfoService;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.service.StudentService;
import com.hjy.attendance.util.BeanUtil;
import com.hjy.attendance.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <p>标题:  Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class CoursePickUpInfoServiceImpl extends BaseServiceImpl<CoursePickUpInfoDao, CoursePickUpInfo> implements CoursePickUpInfoService {

    @Autowired
    StudentService studentService;

    @Override
    public List<Map<String, Object>> getCoursesAndStudents() throws ApiException {
        List<CoursePickUpInfo> lists=list();
        return lists.stream().map(BeanUtil::transBean2Map)
                .peek(data->{
                    data.put("studentName",studentService.getById(data.get("studentNo").toString()).getName());
                }).collect(Collectors.toList());
    }

    @Override
    public List<Map<String, Object>> getMyCourses() throws Exception {
        List<Map<String,Object>> list=this.baseDao.getMyCourses(JwtUtil.decode(IMContextHolder.get("token")));
        return list;
    }

    @Override
    public List<Map<String, Object>> getPickUpListByCourse(String courseNo) throws Exception {
        return this.baseDao.getPickUpListByCourse(courseNo);
    }
}
