package com.hjy.attendance.service.impl;

import com.hjy.attendance.dao.DepartmentDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Department;
import com.hjy.attendance.service.DepartmentService;
import org.springframework.stereotype.Service;


/**
 * <p>标题: 学院表 Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class DepartmentServiceImpl extends BaseServiceImpl<DepartmentDao, Department> implements DepartmentService {

}
