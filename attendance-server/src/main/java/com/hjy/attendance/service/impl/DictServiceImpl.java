package com.hjy.attendance.service.impl;

import com.hjy.attendance.dao.DictDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Dict;
import com.hjy.attendance.service.DictService;
import org.springframework.stereotype.Service;


/**
 * <p>标题:  Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class DictServiceImpl extends BaseServiceImpl<DictDao, Dict> implements DictService {

}
