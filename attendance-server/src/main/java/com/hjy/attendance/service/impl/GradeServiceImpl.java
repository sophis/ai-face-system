package com.hjy.attendance.service.impl;

import com.hjy.attendance.dao.GradeDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Grade;
import com.hjy.attendance.service.GradeService;
import org.springframework.stereotype.Service;


/**
 * <p>标题: 年级表 Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class GradeServiceImpl extends BaseServiceImpl<GradeDao, Grade> implements GradeService {

}
