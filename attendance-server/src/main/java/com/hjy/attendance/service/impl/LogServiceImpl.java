package com.hjy.attendance.service.impl;

import com.alibaba.fastjson.JSON;
import com.hjy.attendance.dao.LogDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Log;
import com.hjy.attendance.service.LogService;
import org.springframework.stereotype.Service;


/**
 * <p>标题:  Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service("logService")
public class LogServiceImpl extends BaseServiceImpl<LogDao, Log> implements LogService {


    @Override
    public int saveLog(String jsonLog) throws Exception {
        Log log= JSON.parseObject(jsonLog,Log.class);
        return this.baseDao.insert(log);
    }


}
