package com.hjy.attendance.service.impl;

import com.hjy.attendance.dao.StudentDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Student;
import com.hjy.attendance.entity.Teacher;
import com.hjy.attendance.entity.User;
import com.hjy.attendance.enums.ResultEnum;
import com.hjy.attendance.exception.ApiException;
import com.hjy.attendance.service.StudentService;
import com.hjy.attendance.service.TeacherService;
import com.hjy.attendance.util.ValidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Validator;
import java.util.Optional;


/**
 * <p>标题:  Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class StudentServiceImpl extends BaseServiceImpl<StudentDao, Student> implements StudentService {

    @Autowired
    TeacherService teacherService;
    @Override
    public Teacher getAssiantByUserName(String userId) throws ApiException {
        Student student= Optional.<Student>ofNullable(getById(userId))
                .orElseThrow(()->new ApiException(ResultEnum.CUSTOMFAILURE,"你的账户已不存在,请联系管理员"));
        String assiantId=student.getAssistant();
        if (ValidUtil.isEmptyOrNull(assiantId)){
            throw new ApiException(ResultEnum.CUSTOMFAILURE,"你当前没有辅导员");
        }
        Teacher teacher=Optional.<Teacher>ofNullable(teacherService.getById(assiantId))
                .orElseThrow(()->new ApiException(ResultEnum.CUSTOMFAILURE,"你的辅导员信息已不存在,请联系管理员"));
        return teacher;
    }
}
