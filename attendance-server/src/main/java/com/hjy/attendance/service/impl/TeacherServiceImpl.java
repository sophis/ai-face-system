package com.hjy.attendance.service.impl;

import com.hjy.attendance.dao.TeacherDao;
import com.hjy.attendance.entity.BaseServiceImpl;
import com.hjy.attendance.entity.Teacher;
import com.hjy.attendance.service.TeacherService;
import org.springframework.stereotype.Service;


/**
 * <p>标题:  Service 实现类</p>
 * <p>描述: </p>
 * <p>版权: Copyright (c) 2020</p>
 *
 * @version: 1.0
 * @author: huangjunyu
 * @date 2020-04-18
 */
@Service
public class TeacherServiceImpl extends BaseServiceImpl<TeacherDao, Teacher> implements TeacherService {

}
