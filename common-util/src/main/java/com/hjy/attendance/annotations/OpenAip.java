package com.hjy.attendance.annotations;


import com.hjy.attendance.util.AipFaceAutoConfigration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @program: springcloud-dependcies
 * @description: 人脸识别控制器
 * @author: xjr
 * @create: 2020-04-19 16:27
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import(AipFaceAutoConfigration.class)
public @interface OpenAip {
    boolean open() default true;
}
