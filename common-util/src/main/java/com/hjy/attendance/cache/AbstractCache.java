package com.hjy.attendance.cache;

import com.hjy.attendance.entity.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @program: attendance-parent
 * @description: 缓存类
 * @author: xjr
 * @create: 2020-04-25 15:46
 **/
@Slf4j
public abstract class AbstractCache<Entity,Service extends BaseService> {
   protected volatile List<Entity> cacheList=new CopyOnWriteArrayList<>();

   @Autowired
   protected Service sourceService;

   protected Class clazz=(Class) (((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[1]);
    //缓存获取
    public abstract List<Entity> get();

    //缓存清空
    public abstract void clear();

    //更新缓存，在数据量比较小的情况下使用
    public abstract void updateById(Entity entity);

    //自定义启动顺序
    public abstract Integer getOrder();

    public void setNull(){
        this.cacheList=null;
    }
    public void init() throws Exception{
     log.info("当前缓存类:{},--------开始执行缓存读取",clazz.getName());
     cacheList=this.sourceService.list();
     log.info("缓存读取完毕------------------------class:{}",clazz.getName());
    }
}
