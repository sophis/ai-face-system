package com.hjy.attendance.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.exceptions.ApiException;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hjy.attendance.cache.AbstractCache;
import com.hjy.attendance.util.FieldUtils;
import com.hjy.attendance.util.CollectionUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;

@ApiResponses({
        @ApiResponse(code = 200,message = "请求成功"),
        @ApiResponse(code = 403,message = "无权限"),
        @ApiResponse(code = 500,message = "服务器内部异常"),
        @ApiResponse(code = 413,message = "客户端崩溃")
})

/**
 * TODO
 *
 * @author xjr
 * @version 1.0
 * @date 2020/2/4 15:35
 */
public class BaseController<Service extends BaseService<Entity>,Entity>{
    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;

    @Autowired
    protected Service service;

    @SuppressWarnings("unchecked")
    protected Class<Entity> entityClass =
            (Class<Entity>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];


    @ApiOperation("根据ID获取单条数据")
    @GetMapping("/{id}")
    public ResultVO<Entity> getById(@PathVariable Serializable id) {
        Entity entity = this.service.getById(id);
        if (entity == null) {
            return ResultVO.failure("找不到指定的记录");
        }
        return ResultVO.data(entity);
    }

    @ApiOperation(value = "根据参数获取单条数据,如果有多条记录,则抛出异常(该已过时,新接口URL不需要加param后缀)",
            notes = "请求示例:\n" +
                    "url : http://api.com/getByParam?fields=age&fields=create_time\n" +
                    "fields为要返回的指定字段,不填默认返回所有字段\n" +
                    "body部分(查询条件) : { id : 10000 , name : jack }\n" +
                    "该请求为查询 id =  10000  , name =  jack  的单条记录 , 并只返回name和age两个字段," +
                    "如果需要返回所有字段,fields参数为空不填即可")
    @GetMapping("/param")
    @Deprecated
    public ResultVO<Entity> getByParam(Entity entity, @RequestParam(required = false) String[] fields) {
        return this.get(entity, fields);
    }

    @ApiOperation(value = "根据参数获取单条数据,如果有多条记录,则抛出异常",
            notes = "请求示例:\n" +
                    "url : http://api.com/getByParam?fields=age&fields=create_time\n" +
                    "fields为要返回的指定字段,不填默认返回所有字段\n" +
                    "body部分(查询条件) : { id : 10000 , name : jack }\n" +
                    "该请求为查询 id =  10000  , name =  jack  的单条记录 , 并只返回name和age两个字段," +
                    "如果需要返回所有字段,fields参数为空不填即可")
    @GetMapping("")
    public ResultVO<Entity> get(Entity entity, @RequestParam(required = false) String[] fields) {
        QueryWrapper<Entity> queryWrapper = this.getEntityQueryWrapper(entity, fields);
        Entity result = this.service.getOne(queryWrapper, true);
        return ResultVO.data(result);
    }

    @ApiOperation(value = "分页查询",
            notes = "注意: 分页参数直接在url后面添加\n\n" +
                    "url: http://api.com/list?username=jack&current=1&size=5&ascs=name&ascs=age&descs=create_time " +
                    "\n\n" +
                    "表示查询第一页前5条数据,并根据名字和年龄正序排序,根据时间倒序排序 \n\n" +
                    "参数说明: \n" +
                    "current = 当前页,默认 1 \n" +
                    "size = 每页显示条数,默认 10 \n" +
                    "ascs = 根据字段正序查询 , 数组格式 , 可选\n" +
                    "descs = 根据字段倒序查询 , 数组格式 , 可选")
    @GetMapping("/list")
    public ResultVO<IPage<Entity>> list(@ApiIgnore Page<Entity> page,
                                        Entity entity,
                                        @RequestParam(required = false) String[] fields) {
        QueryWrapper<Entity> queryWrapper = this.getEntityQueryWrapper(entity, fields);
        IPage<Entity> resultPage = this.service.listByPage(page, queryWrapper);
        return ResultVO.<IPage<Entity>>success().message("获取列表数据成功").data(resultPage).build();
    }

    @ApiOperation(value = "根据单个字段使用 IN 函数查询",
            notes = "例：查询名字为‘jack’和‘rose’的记录\n\n" +
                    "url: http://api.com/list/in?column=STAFF_NAME&values=jack&values=rose\n\n" +
                    "column:列名称，values：值")
    @GetMapping("/list/in")
    public ResultVO<Collection<Entity>> in(String column, String[] values) {
        FieldUtils.checkFieldsThrow(entityClass, Collections.singletonList(column));
        List<Entity> entities = this.service.listIn(column, values);
        return ResultVO.data(entities);
    }

    @ApiOperation("获取所有")
    @GetMapping("/all")
    public ResultVO<Object> all(Entity entity, @RequestParam(required = false) String[] fields) {
        QueryWrapper<Entity> queryWrapper = this.getEntityQueryWrapper(entity, fields);
        List<Entity> resultList = this.service.list(queryWrapper);
        if (fields != null && fields.length > 0) {
            List<Map<String, Object>> data = resultList.stream()
                    .map(BeanUtil::beanToMap)
                    .peek(MapUtil::removeNullValue)
                    .collect(Collectors.toList());
            return ResultVO.data(data);
        } else {
            return ResultVO.data(resultList);
        }
    }

    @ApiOperation(value = "根据字段参数和关键词查询,关键词字段为keywords",
            notes = "请求Body示例:\n" +
                    "{ name : jack , age : 15 , keywords : rose } \n" +
                    "默认模糊查询SPELL_CODE 和 WBZX_CODE 字段 , 如果不需要 , 在url后面加上useDefaultField=false参数即可\n" +
                    "SQL伪代码 where name = 'jack' and age = 15 and (spell_code like '%rose%' or wbzx_code like " +
                    "'%rose%')\n" +
                    "如果需要自定义增加模糊查询的字段,可以在url后面的添加 likeFields 参数\n" +
                    "如: http://api.com/list/keywords?likeFields=field_1&likeFields=field_2 \n" +
                    "此时模糊查询就会增加field1,field2两个字段")
    @GetMapping("/list/keywords")
    public ResultVO<IPage<Entity>> listByKeywords(@ApiIgnore Page<Entity> page,
                                                  Entity entity,
                                                  @RequestParam(required = false) List<String> likeFields,
                                                  @RequestParam(defaultValue = "true") boolean useDefaultField) throws Exception {
        if (!CollectionUtils.isEmpty(likeFields)) {
            FieldUtils.checkFieldsThrow(entity.getClass(), likeFields);
        }
        IPage<Entity> resultList = this.service.listByKeywords(entity, likeFields, useDefaultField, page);
        return ResultVO.data(resultList);
    }


    @ApiOperation("插入数据")
    @PostMapping("/insert")
    public ResultVO<String> insert(@RequestBody @Valid Entity entity) throws ApiException {
        this.service.insert(entity);
        return ResultVO.success("插入数据成功");
    }

    @ApiOperation(value = "批量插入(JSON数组格式),如果已经存在,抛出异常",
            notes = "请求示例:\n" +
                    "url: http://api.com/insertBatch?batchSize=20   其中batchSize为更新批次数量,默认一次10条(可选)\n" +
                    "body部分:\n" +
                    "[ \n" +
                    "   { id : 10000 , name : jack } , \n" +
                    "   { id : 10001 , name : rose } \n" +
                    "]"
    )
    @PostMapping("/insert/batch")
    public ResultVO<String> insertBatch(@RequestBody List<Entity> entityList,
                                        @RequestParam(defaultValue = "10") Integer batchSize) throws ApiException {
        this.service.insertBatch(entityList, batchSize);
        return ResultVO.success("批量插入数据成功");
    }

    @ApiOperation(value = "根据主键更新数据 (默认不更新值为null的字段)",
            notes = "请求Body示例(更新主键ID= 10000 的记录):\n" +
                    "{ id : 10000 , name : jack , age :21}")
    @PostMapping("/update")
    public ResultVO<String> updateById(@RequestBody Entity entity) throws ApiException {
        this.service.updateById(entity);
        return ResultVO.success("更新数据成功");
    }

    @ApiOperation(value = "根据主键批量更新",
            notes = "请求示例:\n" +
                    "url: http://api.com/updateBatch?batchSize=20   其中batchSize为更新批次数量,默认一次10条(可选)\n" +
                    "body部分:\n" +
                    "[ \n" +
                    "   { id : 10000 , name : jack } , \n" +
                    "   { id : 10001 , name : rose } \n" +
                    "]"
    )
    @PostMapping("/update/batch")
    public ResultVO<String> updateBatchById(@RequestBody List<Entity> entityList,
                                            @RequestParam(defaultValue = "10") Integer batchSize) throws ApiException {
        this.service.updateBatchById(entityList, batchSize);
        return ResultVO.success("批量更新数据成功");
    }

    @ApiOperation(value = "根据字段条件更新 (默认不更新值为null的字段)",
            notes = "请求Body示例:\n" +
                    "{\n" +
                    " oldInfo :{ id : 10000 , name : jack }\n" +
                    " newInfo :{ name : rose , age :21}\n" +
                    "}")
    @PostMapping("/update/param")
    public ResultVO<String> update(@RequestBody EntityWrapper<Entity> entityWrapper) throws ApiException {
        this.service.updateByParam(entityWrapper);
        return ResultVO.success("更新成功");
    }

    @ApiOperation(value = "根据字段条件更新 (默认不更新值为null的字段)",
            notes = "请求Body示例:\n" +
                    "[{\n" +
                    " oldInfo :{ id : 10000 , name : jack }\n" +
                    " newInfo :{ name : rose , age :21}\n" +
                    "}]")
    @PostMapping("/update/batch/param")
    public ResultVO<String> updateBatch(@RequestBody List<EntityWrapper<Entity>> entityList) throws ApiException {
        this.service.updateBatch(entityList);
        return ResultVO.success("更新成功");
    }

    @ApiOperation(value = "保存数据,存在则更新,不存在则插入",
            notes = "请求Body示例:\n" +
                    "{ id : 10000 , name : jack , age :21}\n" +
                    "当存在主键ID= 10000 的记录时,更新该记录,否则根据实体参数插入一条ID= 10000 的记录")
    @PostMapping("/save")
    public ResultVO<String> save(@RequestBody @Valid Entity entity) throws ApiException {
        this.service.insertOrUpdate(entity);
        return ResultVO.success("保存成功");
    }

    @ApiOperation(value = "批量修改插入(JSON数组格式),存在则更新,不存在则插入",
            notes = "请求示例:\n" +
                    "url: http://api.com/insertBatch?batchSize=20   其中batchSize为更新批次数量,默认一次10条(可选)\n" +
                    "body部分:\n" +
                    "[ \n" +
                    "   { id : 10000 , name : jack } , \n" +
                    "   { id : 10001 , name : rose } \n" +
                    "]\n" +
                    "说明:如果id= 10000 的记录存在,而id= 10001 的记录不存在,则会更新id= 10000 的记录,插入一条id= 10001 的记录")
    @PostMapping("/save/batch")
    public ResultVO<String> saveBatch(@RequestBody List<Entity> entityList,
                                      @RequestParam(defaultValue = "10") Integer batchSize) throws ApiException {
        this.service.insertOrUpdateBatch(entityList, batchSize);
        return ResultVO.success("批量保存数据成功");
    }

    @ApiOperation("根据ID删除数据")
    @DeleteMapping("/{id}")
    public ResultVO<String> deleteById(@PathVariable Serializable id) throws Exception {
        this.service.deleteById(id);
        return ResultVO.success("删除成功");
    }

    @ApiOperation(value = "根据参数(JSON格式)删除数据,只能删除单条记录,如果删除结果不等于1,则抛出异常",
            notes = "请求Body示例(更新主键ID= 10000 的记录):\n" +
                    "{ id : 10000 , name : jack}\n" +
                    "说明:删除id = 10000 , name = jack 的记录")
    @DeleteMapping("/one")
    public ResultVO<String> deleteOne(@RequestBody Entity entity) throws ApiException {
        QueryWrapper<Entity> queryWrapper = new QueryWrapper<>(entity);
        this.service.deleteOne(queryWrapper);
        return ResultVO.success("删除数据成功");
    }

    @ApiOperation(value = "根据ID批量删除数据,只要删除成功的记录数超过一条就视为操作成功",
            notes = "说明: 只要删除成功的记录数超过一条就视为操作成功,比如删除3条记录,返回的数量大于1,便视为删除成功,\n" +
                    "请求Body示例 : [ 100 , 101 , 102 ] ")
    @DeleteMapping("/batch")
    public ResultVO<String> deleteBatchIds(@RequestBody List<String> ids) throws ApiException {
        this.service.deleteByIds(ids);
        return ResultVO.success("批量删除成功");
    }

    @ApiOperation(value = "批量删除(JSON数组格式)",
            notes = "请求示例:\n" +
                    "url: http://api.com/deleteBatch?batchSize=20   其中batchSize为更新批次数量,默认一次10条(可选)\n" +
                    "body部分:\n" +
                    "[ \n" +
                    "   { id : 10000 , name : jack } , \n" +
                    "   { id : 10001 , name : rose } \n" +
                    "]"
    )
    @DeleteMapping("/batch/param")
    public ResultVO<String> deleteBatch(@RequestBody List<Entity> entityList,
                                        @RequestParam(defaultValue = "10") Integer batchSize) throws ApiException {
        this.service.deleteBatch(entityList, batchSize);
        return ResultVO.success("批量删除数据成功");
    }

    protected QueryWrapper<Entity> getEntityQueryWrapper(Entity entity,
                                                         @RequestParam(required = false) String[] fields) {
        QueryWrapper<Entity> queryWrapper = new QueryWrapper<>();
        if (entity != null) {
            queryWrapper.setEntity(entity);
        }
        if (fields != null && fields.length > 0) {
            FieldUtils.checkFieldsThrow(entityClass, Arrays.asList(fields));
            queryWrapper.select(fields);
        }
        return queryWrapper;
    }
}
