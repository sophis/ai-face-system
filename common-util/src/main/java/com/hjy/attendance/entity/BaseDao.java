package com.hjy.attendance.entity;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.io.Serializable;

/**
 * TODO
 *
 * @author xjr
 * @version 1.0
 * @date 2020/2/4 14:45
 */
public interface BaseDao<Entity> extends BaseMapper<Entity> {
    Integer existsById(Serializable id);
}
