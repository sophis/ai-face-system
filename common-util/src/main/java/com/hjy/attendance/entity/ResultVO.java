package com.hjy.attendance.entity;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.enums.ResultEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @version: 1.0
 * @author: xiejiarong
 * @date 2020/03/18
 */
@Data
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("返回数据")
public class ResultVO<T> implements Serializable {

    @ApiModelProperty("返回结果状态编码")
    private String code;
    @ApiModelProperty("错误信息")
    private String error;
    @ApiModelProperty("返回消息")
    private String message;
    @ApiModelProperty("返回数据")
    private T data;
    @ApiModelProperty("客户端令牌")
    private String token;

    public static <T> ResultVO<T> success(String message) {
        return ResultVO.<T>builder().code(ResultEnum.SUCCESS.getCode()).message(message).token(tokenGet()).build();
    }

    public static <T> ResultVOBuilder<T> success() {
        return ResultVO.<T>builder().code(ResultEnum.SUCCESS.getCode());
    }

    public static <T> ResultVO<T> failure(String message) {
        return ResultVO.<T>builder().code(ResultEnum.FAILURE.getCode()).message(message).token(tokenGet()).build();
    }

    public static <T> ResultVO<T> failure(String code, String message) {
        return ResultVO.<T>builder().code(code).message(message).token(tokenGet()).build();
    }

    public static <T> ResultVOBuilder<T> failure() {
        return ResultVO.<T>builder().code(ResultEnum.FAILURE.getCode());
    }

    public static <T> ResultVO<T> data(T data) {
        return ResultVO.<T>success().data(data).token(tokenGet()).build();
    }
    public static String tokenGet(){
        return IMContextHolder.get(IMContextHolder.TOKEN);
    }
}
