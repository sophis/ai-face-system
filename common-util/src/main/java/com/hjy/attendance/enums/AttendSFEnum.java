package com.hjy.attendance.enums;

/**
 * 请假成功失败枚举类
 */
public enum AttendSFEnum {

	SUCCESS(1, "成功"),
	FAIL(0, "失败");

	public final Integer type;
	public final String value;

	AttendSFEnum(Integer type, String value){
		this.type = type;
		this.value = value;
	}

}
