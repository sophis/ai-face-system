package com.hjy.attendance.enums;

/**
 * 考勤标识 0 迟到 1正常 2 缺勤 3请假
 */
public enum AttentanceEnum {

	LATE(0, "迟到"),
	NORMAL(1, "正常"),
	DUTY(2, "缺勤"),
	LEAVE(3, "请假");

	public final Integer type;
	public final String value;

	AttentanceEnum(Integer type, String value){
		this.type = type;
		this.value = value;
	}

}
