package com.hjy.attendance.enums;

/**
 * 审批状态 0 未处理 1 批准 2 驳回
 */
public enum ValidEnum {

	UNPROCESSED(0, "未处理"),
	AGREE(1, "同意"),
	DISAGREE(2, "驳回");


	public final Integer type;
	public final String value;

	ValidEnum(Integer type, String value){
		this.type = type;
		this.value = value;
	}

}
