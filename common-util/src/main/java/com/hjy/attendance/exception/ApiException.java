package com.hjy.attendance.exception;

import com.hjy.attendance.enums.ResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @version: 1.0
 * @author: xiejiarong
 * @date 2020/03/18
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ApiException extends Exception {

    private static final long serialVersionUID = 5722486780178759094L;
    private ResultEnum resultEnum;

    public ApiException(String message) {
        super(message);
        this.resultEnum = ResultEnum.FAILURE;
    }

    public ApiException(ResultEnum resultEnum, String message){
        super(message);
        this.resultEnum = resultEnum;
    }

}
