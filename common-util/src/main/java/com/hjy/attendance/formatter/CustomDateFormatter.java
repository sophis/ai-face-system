package com.hjy.attendance.formatter;


import com.hjy.attendance.util.DateConverterUtils;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;


/**
 *
 * @version: 1.0
 * @author: xiejiarong
 * @date: 2020/03/18
 */
public class CustomDateFormatter implements Formatter<Date> {
    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        if ("".equals(text) || "null".equals(text)) {
            return null;
        }
        return DateConverterUtils.convert(text);
    }

    @Override
    public String print(Date object, Locale locale) {
        return String.valueOf(object.getTime());
    }
}
