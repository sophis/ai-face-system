package com.hjy.attendance.init;

import com.hjy.attendance.cache.AbstractCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: attendance-parent
 * @description: 缓存初始化
 * @author: xjr
 * @create: 2020-04-25 16:29
 **/
@Component
@Slf4j
@Order(-100)
public class CacheInitManager implements CommandLineRunner {

    @Autowired
    List<AbstractCache> caches;

    @Override
    public void run(String... args) throws Exception {
        log.info("全局缓存初始化开始-------");
        caches=caches.stream().sorted(Comparator.comparing(AbstractCache::getOrder)).collect(Collectors.toList());
        caches.stream().forEach(cache->{
            try {
                cache.init();
            } catch (Exception e) {
                log.info("缓存执行出错,错误的类是：{}，具体原因请看:{}",cache.getClass().getName(),e.getMessage());
            }
        });
    }
}
