package com.hjy.attendance.monitor;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.util.IPUtil;
import io.undertow.servlet.attribute.ServletRequestAttribute;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.actuate.autoconfigure.web.server.LocalManagementPort;
import org.springframework.cglib.core.Local;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: attendance-parent
 * @description: 接口检测日志
 * @author: xjr
 * @create: 2020-04-20 17:02
 **/
@Component
@Aspect
@Slf4j
public class RestMonitor implements ApplicationContextAware, InitializingBean {

    private static volatile BeanFactory beanFactory;

    private static volatile Object object;

    private static  Map<String,Object> logMap=new ConcurrentHashMap<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        beanFactory=applicationContext;
        object=beanFactory.getBean("logService");
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("当前日志检测功能已开启,日志服务类信息：{}",this.object.toString());
    }

    @Pointcut("@annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void pointCut(){
    }

//    @Around("pointCut()")
//    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
//        MethodSignature methodSignature= (MethodSignature) proceedingJoinPoint.getSignature();
//        Method targetMethod=methodSignature.getMethod();
//        HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
//        logMap.put("userName", IMContextHolder.get(IMContextHolder.OPERATOR));
//        logMap.put("logContent","调用method方法"+targetMethod.getName());
//        logMap.put("recordTime",new Date());
//        logMap.put("ipv4Address", IPUtil.getIpAddr(request));
//        LocalTime startTime= LocalTime.now();
//        Object result=proceedingJoinPoint.proceed();
//        LocalTime endTime=LocalTime.now();
//        logMap.put("spendTime", String.valueOf(Duration.between(endTime,startTime).toMillis()));
//        Method method=object.getClass().getDeclaredMethod("saveLog");
//        Integer effetcLog=(Integer)method.invoke(object, JSON.toJSONString(logMap));
//        if (effetcLog>0){
//            log.info("日志记录成功,{}",logMap);
//        }
//        return result;
//    }


}
