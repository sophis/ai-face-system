package com.hjy.attendance.util;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO
 *
 * @author xjr
 * @version 1.0
 * @date 2020/2/4 14:59
 */
public class BeanUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(BeanUtil.class);

    private BeanUtil() {
    }

    public static void transMap2Bean2(Map<String, Object> map, Object obj) {
        if (map != null && obj != null) {
            try {
                BeanUtils.populate(obj, map);
            } catch (IllegalAccessException var3) {
                LOGGER.error("IllegalAccessException", var3);
            } catch (InvocationTargetException var4) {
                LOGGER.error("InvocationTargetException", var4);
            }

        }
    }

    public static void transMap2Bean(Map<String, Object> map, Object obj) {
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            PropertyDescriptor[] var4 = propertyDescriptors;
            int var5 = propertyDescriptors.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                PropertyDescriptor property = var4[var6];
                String key = property.getName();
                if (map.containsKey(key)) {
                    Object value = map.get(key);
                    Method setter = property.getWriteMethod();
                    setter.invoke(obj, value);
                }
            }
        } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException var11) {
            LOGGER.error("context", var11);
        } catch (IntrospectionException var12) {
            LOGGER.error("IntrospectionException", var12);
        }

    }

    public static Map<String, Object> transBean2Map(Object obj) {
        if (obj == null) {
            return null;
        } else {
            HashMap map = new HashMap();

            try {
                BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
                PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
                PropertyDescriptor[] var4 = propertyDescriptors;
                int var5 = propertyDescriptors.length;

                for(int var6 = 0; var6 < var5; ++var6) {
                    PropertyDescriptor property = var4[var6];
                    String key = property.getName();
                    if (!"class".equals(key)) {
                        Method getter = property.getReadMethod();
                        Object value = getter.invoke(obj);
                        map.put(key, value);
                    }
                }
            } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException var11) {
                LOGGER.error("context", var11);
            } catch (IntrospectionException var12) {
                LOGGER.error("IntrospectionException", var12);
            }

            return map;
        }
    }
}
