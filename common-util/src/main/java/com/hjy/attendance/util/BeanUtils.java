package com.hjy.attendance.util;

import cn.hutool.core.map.MapUtil;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.*;

/**
 * @program: springcloud-project
 * @description: bean工具类
 * @author: xjr
 * @create: 2020-03-09 22:17
 **/

public class BeanUtils extends org.springframework.beans.BeanUtils {
    private static final String SERIAL_VERSION_UID = "serialVersionUID";

    /**
     * 获取值为 Null 的字段
     *
     * @param source 对象实体
     * @return
     */
    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    /**
     * 拷贝字段,字段值为空则不拷贝
     *
     * @param src    原始字段
     * @param target 目标字段
     */
    public static void copyPropertiesIgnoreNull(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    /**
     * 拷贝字段,字段值为空则不拷贝
     *
     * @param src         原始字段
     * @param targetClass 目标字段类型
     */
    public static <T> T copyPropertiesIgnoreNull(Object src, Class<T> targetClass) {
        try {
            T instance = targetClass.newInstance();
            BeanUtils.copyProperties(src, instance, getNullPropertyNames(src));
            return instance;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 实体类转map
     *
     * @param obj 实体类
     * @return
     * @throws Exception
     */
    public static Map<String, Object> objectToMap(Object obj, boolean ignoreNullValue) throws IllegalAccessException {
        if (obj == null) {
            return Collections.emptyMap();
        }

        Map<String, Object> map = new HashMap<>(4);

        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            String key = field.getName();
            if (key.equalsIgnoreCase(SERIAL_VERSION_UID)) {
                continue;
            }
            Object value = field.get(obj);
            if (ignoreNullValue && value == null) {
                continue;
            }
            map.put(key, value);
        }

        return map;
    }

    public static Map<String, Object> objectToMap(Object obj) throws IllegalAccessException {
        return objectToMap(obj, false);
    }

    /**
     * 将字节数组转换为对象
     */
    public static Object byteToBean(byte[] bytes) {
        Object obj = null;
        try {
            ByteArrayInputStream bi = new ByteArrayInputStream(bytes);
            ObjectInputStream oi = new ObjectInputStream(bi);

            obj = oi.readObject();
            bi.close();
            oi.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

    /**
     * 将对像转换为字节
     */
    public static byte[] objectToByte(Object obj) {
        byte[] bytes = null;
        try {
            // object to bytearray
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            ObjectOutputStream oo = new ObjectOutputStream(bo);
            oo.writeObject(obj);

            bytes = bo.toByteArray();

            bo.close();
            oo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }

    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        }
        try {
            Map<String, Object> paramMap = BeanUtils.objectToMap(obj);
            Map<String, Object> map = MapUtil.removeNullValue(paramMap);
            return map.size() == 0;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return true;
        }
    }
}
