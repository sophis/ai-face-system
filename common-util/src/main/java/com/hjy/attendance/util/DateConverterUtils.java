package com.hjy.attendance.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @version: 1.0
 * @author: xiejiarong
 * @date: 03/18/2020
 */
public class DateConverterUtils {

    private static final List<String> FORMATS = new ArrayList<>(4);

    static {
        FORMATS.add("yyyy-MM");
        FORMATS.add("yyyy-MM-dd");
        FORMATS.add("yyyy-MM-dd hh:mm");
        FORMATS.add("yyyy-MM-dd hh:mm:ss");
        FORMATS.add("yyyy-MM-dd-hh");
        FORMATS.add("yyyy-MM-dd-hh-mm");
        FORMATS.add("yyyy-MM-dd-hh-mm-ss");
    }

    public static Date convert(String source) {

        if (StringUtils.isEmpty(source)) {
            return null;
        }
        String value = source.trim();
        if ("".equals(value)) {
            return null;
        }
        try {
            if (source.matches("\\d+")) {
                if (value.length() == 10) {
                    long l = Long.parseLong(value) * 1000;
                    return new Date(l);
                }
                return new Date(Long.parseLong(value));
            } else if (source.matches("^\\d{4}-\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(0));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(1));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(2));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(3));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}-{1}\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(4));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}-{1}\\d{1,2}-\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(5));
            } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}-{1}\\d{1,2}-\\d{1,2}-\\d{1,2}$")) {
                return parseDate(source, FORMATS.get(6));
            } else {
                throw new IllegalArgumentException("无效的日期格式:" + source);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 格式化日期
     *
     * @param dateStr String 字符型日期
     * @param format  String 格式
     * @return Date 日期
     */
    private static Date parseDate(String dateStr, String format) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.parse(dateStr);
    }
}
