package com.hjy.attendance.util;

import java.io.File;

/**
 * @program: springcloud-project
 * @description: 文件工具类
 * @author: xjr
 * @create: 2020-03-15 18:58
 **/

public class FileUtil {
    public synchronized static boolean delFile(File file) {
        if (!file.exists()) {
            return false;
        }

        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                delFile(f);
            }
        }
        return file.delete();
    }
}
