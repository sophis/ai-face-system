package com.hjy.attendance.util;/*
 *@date 2020/1/30
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.hjy.attendance.context.IMContextHolder;
import com.hjy.attendance.exception.ApiException;

import java.util.Date;
import java.util.HashMap;
import java.util.function.Consumer;

/**
 * @author xjr
 */
public class JwtUtil {
    /**
     * 过期时间为一天
     * TODO 正式上线更换为15分钟
     */
    private static final long EXPIRE_TIME = 60*1000*60*24;

    /**
     * token私钥
     */
    private static final String TOKEN_SECRET = "encryptUserId";

    /**
     * 生成签名,15分钟后过期
     * @param userId
     * @return
     */
    public static String sign(String userId){
        //过期时间
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        //私钥及加密算法
        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
        //设置头信息
        HashMap<String, Object> header = new HashMap<>(2);
        header.put("typ", "JWT");
        header.put("alg", "HS256");
        //附带username和userID生成签名
        return JWT.create().withHeader(header)
                .withClaim("userId",userId).withExpiresAt(date).sign(algorithm);
    }


    public static boolean verity(String token){
        try {
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        } catch (JWTVerificationException e) {
            return false;
        }

    }

    public static String decode(String token)throws ApiException{
        if (!verity(token)){
            throw new ApiException("token已经过期，请重新登录");
        }
        return JWT.decode(token).getClaim("userId").asString();
    }

    public static void main(String[] args) {
        System.out.println(JwtUtil.sign("xiaoxie"));
    }

    public static void setGlobalEmptyToken(){
        IMContextHolder.set(IMContextHolder.TOKEN,"");
        IMContextHolder.set(IMContextHolder.OPERATOR,"");
    }

}
