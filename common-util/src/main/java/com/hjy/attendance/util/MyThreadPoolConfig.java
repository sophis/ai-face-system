package com.hjy.attendance.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import sun.nio.ch.ThreadPool;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @program: home-server
 * @description: 定时任务
 * @author: xjr
 * @create: 2020-02-21 14:14
 **/
@Configuration
@EnableAsync
@Slf4j
public class MyThreadPoolConfig {

    @Bean
    ThreadPoolTaskExecutor threadPoolExecutor(){
        log.info("线程池配置生效,配置类信息:{}",this.getClass().getName());
        ThreadPoolTaskExecutor threadPoolTaskExecutor=new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(5);
        threadPoolTaskExecutor.setMaxPoolSize(10);
        threadPoolTaskExecutor.setQueueCapacity(10);
        threadPoolTaskExecutor.setKeepAliveSeconds(60);
        threadPoolTaskExecutor.setThreadNamePrefix("xie-");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }
}
