package com.hjy.attendance.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @version: 1.0
 * 反射工具类
 * @author: xiejiarong
 * @date: 2020/03/09
 */
public class ReflectUtils {

    public static List<Class> getClassAnnotationClasses(Class clazz) {
        // 虽然这个方法，返回值说的是Type ，
        // 但是其实返回的是ParameterizedType的实现类类型。
        // 所以我们使用ParameterizedTypeImpl接口来接收。
        Type type = clazz.getGenericSuperclass();
        if (!(type instanceof ParameterizedType)) {
            return Collections.emptyList();
        }
        ParameterizedType genericSuperclass = (ParameterizedType) type;
        // 获取这样可以得到泛型了
        // 因为泛型可能不止一个,所以返回的是数组,所以我们取第一个,
        Type[] types = genericSuperclass.getActualTypeArguments();
        if (types == null || types.length == 0) {
            return Collections.emptyList();
        }
        return Arrays.stream(types).map(t -> (Class) t).collect(Collectors.toList());
    }

}
